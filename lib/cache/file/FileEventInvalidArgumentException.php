<?php

use Psr\Cache\InvalidArgumentException;

class FileEventInvalidArgumentException extends Exception implements InvalidArgumentException
{

}