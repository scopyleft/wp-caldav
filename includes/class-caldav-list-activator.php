<?php

/**
 * Fired during plugin activation
 *
 * @link       https://enesnet.de
 * @since      1.0.0
 *
 * @package    Caldav_List
 * @subpackage Caldav_List/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Caldav_List
 * @subpackage Caldav_List/includes
 * @author     Oliver Enes <oliver@enesnet.de>
 */
class Caldav_List_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
